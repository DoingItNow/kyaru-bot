"""
    Initialise the DB tables if they do not exist
"""
import os
import psycopg2

DATABASE_URL = os.environ['DATABASE_URL']


def create_server_table():
    """ create tables in the PostgreSQL database"""
    command = (
        """ CREATE TABLE servers (
                server_id BIGINT PRIMARY KEY,
                schedule_enabled TEXT,
                post_delete TEXT,
                queue_setup TEXT,
                queue_type TEXT,
                reset_time TIME,
                start_date DATE,
                end_date DATE
            )
        """
    )

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cur = conn.cursor()
        # create table one by one
        cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()

        print("Servers Table have been created")
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def create_messages_table():
    """ create tables in the PostgreSQL database"""
    command = (
        """ CREATE TABLE messages (
                message_id BIGINT PRIMARY KEY,
                server_id BIGINT,
                queue_number INTEGER,
                queue_type TEXT,
                FOREIGN KEY (server_id) REFERENCES servers (server_id)
            )
        """
    )

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cur = conn.cursor()
        # create table one by one
        cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()

        print("Messages Table have been created")
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def create_boss_queue_tables():
    """ create tables in the PostgreSQL database"""
    commands = (
        """ CREATE TABLE boss1 (
                id serial PRIMARY KEY,
                server_id BIGINT,
                user_name TEXT,
                queue_number INTEGER,
                queue_type TEXT,
                date DATE,
                timestamp TIME
                )
        """,
        """ CREATE TABLE boss2 (
                id serial PRIMARY KEY,
                server_id BIGINT,
                user_name TEXT,
                queue_number INTEGER,
                queue_type TEXT,
                date DATE,
                timestamp TIME
                )
        """,
        """ CREATE TABLE boss3 (
                id serial PRIMARY KEY,
                server_id BIGINT,
                user_name TEXT,
                queue_number INTEGER,
                queue_type TEXT,
                date DATE,
                timestamp TIME
                )
        """,
        """ CREATE TABLE boss4 (
                id serial PRIMARY KEY,
                server_id BIGINT,
                user_name TEXT,
                queue_number INTEGER,
                queue_type TEXT,
                date DATE,
                timestamp TIME
                )
        """,
        """ CREATE TABLE boss5 (
                id serial PRIMARY KEY,
                server_id BIGINT,
                user_name TEXT,
                queue_number INTEGER,
                queue_type TEXT,
                date DATE,
                timestamp TIME
                )
        """)

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()

        print("Boss Queue Tables have been created")
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def create_role_table():
    """ create tables in the PostgreSQL database"""
    command = (
        """ CREATE TABLE bossrole (
                server_id BIGINT PRIMARY KEY,
                boss_role TEXT,
                day_number INTEGER
            )
        """
    )

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cur = conn.cursor()
        # create table one by one
        cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()

        print("Boss Role Table have been created")
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    create_server_table()
    create_boss_queue_tables()
    create_messages_table()
    create_role_table()
