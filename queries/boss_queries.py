"""
Functions containing DB queries
"""
import os
from datetime import date
from datetime import datetime
import psycopg2

DATABASE_URL = os.environ['DATABASE_URL']


def insert_user_entry(boss_number, server_id, user_name, queue_number, queue_type):
    """ insert a new user into the boss table """
    current_time = datetime.strftime(datetime.now(), '%H:%M:%S')
    current_date = datetime.strftime(date.today(), '%Y-%m-%d')

    current_time = "'%s'" % current_time
    current_date = "'%s'" % current_date

    sql_query = "INSERT INTO {} (server_id, user_name, queue_number, queue_type, date, timestamp)  VALUES({},{},{},{},{},{});".format(boss_number, server_id, user_name, queue_number, queue_type, current_date, current_time)

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        cursor.execute(sql_query)

        print("The new user that was inserted into the user queue table is")
        sql_select_query = "SELECT * FROM {} WHERE user_name = {} AND queue_number = {};".format(boss_number, user_name, queue_number)
        cursor.execute(sql_select_query)

        boss_user_record = cursor.fetchone()
        print(boss_user_record)

        # commit the changes to the database and close
        conn.commit()
        cursor.close()
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_users_from_boss(boss_number, server_id, queue_number):
    """
        Get a users id from the queue table
    """
    sql_query = "SELECT user_name, to_char(timestamp, 'HH24:MI') FROM {} WHERE queue_number = {} AND server_id = {} order by timestamp ASC;".format(boss_number, queue_number, server_id)

    conn = None
    role_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)
        query_count = cursor.rowcount

        if query_count > 0:
            role_attribute = cursor.fetchall()

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

        return role_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete_user_queue(boss_number, server_id, user_name, queue_number):
    """
        Delete row from the queue table based from day or round number and user name
    """
    sql_query = "DELETE FROM {} WHERE user_name = {} AND queue_number ={} AND server_id = {};".format(boss_number, user_name, queue_number, server_id)

    conn = None
    server_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)

        # commit the changes to the database and close database
        conn.commit()

        count = cursor.rowcount
        print(count, "User from " + str(boss_number) + " has been deleted successfully.")
        cursor.close()

        return server_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete_queue_daily(boss_number, server_id, queue_number):
    """
        Delete row from the queue table based from round or day only
    """
    sql_query = "DELETE FROM {} WHERE queue_number = {} AND server_id = {};".format(boss_number, queue_number, server_id)

    conn = None
    server_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)

        # commit the changes to the database and close database
        conn.commit()

        count = cursor.rowcount
        print(count, "Users from " + str(boss_number) + " has been deleted successfully.")
        cursor.close()

        return server_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete_queue_reset(server_id):
    """
        Delete row from the queue table based from round number only
    """
    sql_query_commands = ("DELETE FROM boss1 WHERE server_id = {};".format(server_id),
                          "DELETE FROM boss2 WHERE server_id = {};".format(server_id),
                          "DELETE FROM boss3 WHERE server_id = {};".format(server_id),
                          "DELETE FROM boss4 WHERE server_id = {};".format(server_id),
                          "DELETE FROM boss5 WHERE server_id = {};".format(server_id))

    conn = None
    server_attribute = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        for command in sql_query_commands:
            cursor.execute(command)
            count = cursor.rowcount
            print(count, "Deleted boss table")

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

        return server_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
