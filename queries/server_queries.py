"""
Functions containing DB queries
"""
import os
import psycopg2

DATABASE_URL = os.environ['DATABASE_URL']


def insert_servers_entry(server_id, schedule_enabled, queue_setup, queue_type, post_delete, reset_time):
    """ insert a new server into the server table """
    sql_query = "INSERT INTO servers(server_id, schedule_enabled, queue_setup, queue_type, post_delete, reset_time) VALUES({},{},{},{},{},{});".format(
        server_id, schedule_enabled, queue_setup, queue_type, post_delete, reset_time
    )
    conn = None

    returned_server_id = get_server_attribute('server_id', server_id)
    if returned_server_id is None:
        try:
            # connect to the PostgreSQL server
            conn = psycopg2.connect(DATABASE_URL, sslmode='require')
            cursor = conn.cursor()
            cursor.execute(sql_query)

            print("The new server that was inserted into the server table is")
            sql_select_query = "SELECT * FROM servers WHERE server_id = {};".format(server_id)
            cursor.execute(sql_select_query)

            server_record = cursor.fetchone()
            print(server_record)

            # commit the changes to the database and close
            conn.commit()
            cursor.close()

        except (psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
    else:
        print("Server ID already exists in the server table")


def get_server_attribute(server_table_attribute, server_id):
    """
        Get a server attribute from the servers table
    """
    sql_query = "SELECT {} FROM servers WHERE server_id = {};".format(server_table_attribute, server_id)

    conn = None
    server_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)
        query_count = cursor.rowcount

        if query_count > 0:
            server_attribute = cursor.fetchone()[0]

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

        return server_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def update_server_attribute(server_table_attribute, attribute_var, server_id):
    """
        Update the server attribute from the servers table
    """
    sql_query = "UPDATE servers SET {} = {} WHERE server_id = {};".format(server_table_attribute, attribute_var, server_id)
    conn = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)

        print("Table After updating server attribute ")
        sql_select_query = "SELECT * FROM servers WHERE server_id = {};".format(server_id)
        cursor.execute(sql_select_query)

        server_record = cursor.fetchone()
        print(server_record)

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
