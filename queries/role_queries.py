"""
Functions containing DB queries
"""
import os
import psycopg2

DATABASE_URL = os.environ['DATABASE_URL']


def insert_role_entry(role, server_id, day_number):
    """ insert a new role into the boss role table """
    sql_query = "INSERT INTO bossrole(boss_role, server_id, day_number)  VALUES({},{},{});".format(role, server_id, day_number)

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        cursor.execute(sql_query)

        print("The current role that was inserted into the boss role table is")
        sql_select_query = "SELECT * FROM bossrole WHERE server_id = {};".format(server_id)
        cursor.execute(sql_select_query)

        boss_role_record = cursor.fetchone()
        print(boss_role_record)

        # commit the changes to the database and close
        conn.commit()
        cursor.close()
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_role_attribute(server_id, role_attribute):
    """ get a role_attribute from the boss role table """
    sql_select_query = "SELECT {} FROM bossrole WHERE server_id = {};".format(role_attribute, server_id)

    conn = None
    role_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_select_query)
        query_count = cursor.rowcount

        if query_count > 0:
            role_attribute = cursor.fetchone()[0]

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

        return role_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def update_role_entry(role_attribute, attribute_var, server_id):
    """ update a new role into the boss role table """
    sql_query = "UPDATE bossrole SET {} = {} WHERE server_id = {};".format(role_attribute, attribute_var, server_id)

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        cursor.execute(sql_query)

        print("Table After updating bossrole ")
        sql_select_query = "SELECT * FROM bossrole WHERE server_id = {};".format(server_id)
        cursor.execute(sql_select_query)

        boss_role_record = cursor.fetchone()
        print(boss_role_record)

        # commit the changes to the database and close
        conn.commit()
        cursor.close()
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete_role_entry(server_id):
    """ deletes role into the boss role table """
    sql_query = "DELETE FROM bossrole WHERE server_id = {};".format(server_id)

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)

        # commit the changes to the database and close database
        conn.commit()

        count = cursor.rowcount
        print(count, "Current Boss Role has been deleted successfully.")
        cursor.close()

        # commit the changes to the database and close
        conn.commit()
        cursor.close()
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
