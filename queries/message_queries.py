"""
Functions containing DB queries
"""
import os
import psycopg2

DATABASE_URL = os.environ['DATABASE_URL']


def insert_messages_round_entry(message_id, server_id, queue_number, queue_type):
    """ insert a new vendor into the vendors table """
    sql_query = "INSERT INTO messages(message_id, server_id, queue_number, queue_type) VALUES({},(SELECT server_id FROM servers WHERE server_id = {}), {}, {});".format(message_id, server_id, queue_number, queue_type)

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        cursor.execute(sql_query)

        print("The new message that was inserted into the messages table is")
        sql_select_query = "SELECT * FROM messages WHERE message_id = {};".format(message_id)
        cursor.execute(sql_select_query)

        message_record = cursor.fetchone()
        print(message_record)

        # commit the changes to the database and close
        conn.commit()
        cursor.close()
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_reset_time(server_id):
    """
        Get a server attribute from the servers table
    """
    sql_query = "SELECT to_char(reset_time, 'HH24:MI') FROM servers WHERE server_id = {};".format(server_id)

    conn = None
    server_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)
        query_count = cursor.rowcount

        if query_count > 0:
            server_attribute = cursor.fetchone()[0]

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

        return server_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_react_message_id(message_id):
    """
        Get a message id from the messages table
    """
    sql_query = "SELECT message_id FROM messages WHERE message_id = {};".format(message_id)

    conn = None
    server_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)
        query_count = cursor.rowcount

        if query_count > 0:
            server_attribute = cursor.fetchone()[0]

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

        return server_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_queue_message_id(server_id, queue_number):
    """
        Get a message id from the messages table based off the queue_number (either day or number)
    """
    sql_query = "SELECT message_id FROM messages WHERE server_id = {} AND queue_number = {};".format(server_id, queue_number)

    conn = None
    server_attribute = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)
        query_count = cursor.rowcount

        if query_count > 0:
            server_attribute = cursor.fetchone()[0]

        # commit the changes to the database and close database
        conn.commit()
        cursor.close()

        return server_attribute
    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete_messages_from_server(server_id):
    """
        Delete message ids from the messages table based from server ID
    """
    sql_query = "DELETE FROM messages WHERE server_id = {};".format(server_id)

    conn = None

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cursor = conn.cursor()

        # execute the INSERT statement
        cursor.execute(sql_query)

        # commit the changes to the database and close database
        conn.commit()

        count = cursor.rowcount
        print(count, "Messages from the Server ID: " + str(server_id) + " has been deleted successfully.")
        cursor.close()

    except (psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
