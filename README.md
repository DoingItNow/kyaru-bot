# Kyaru Bot
This is a bot for the game Princess Connect:ReDive that manages queuing for bosses in Clan Battles

Some of the features include:
* Creates boss roles to be used for Clan Battles
* Sends a message with reactions which assign roles to clan members
* Ping and list the boss roles
* Automated cleanup of roles based on marking roles as "dead"

## How to authorise bot to join your server
* Kyaru (Prod): https://discordapp.com/oauth2/authorize?client_id=563569209796460564&scope=bot&permissions=1544023152
* Summer Kyaru (Dev): https://discordapp.com/oauth2/authorize?client_id=563569267325534220&scope=bot&permissions=1544023152

