"""
Main module for Kyaru Bot
"""
import discord
from discord.ext import commands

from config import setup
from config import config

from queries import server_queries

EXTENSIONS = [
    'cogs.boss_queue',
    'cogs.boss_tracking',
    'cogs.reactions',
    'cogs.scheduler',
    'cogs.help_messages',
    'cogs.channel_setup',
    'cogs.gif_messages'
]

CREDS = config.__token__


class KyaruBot(commands.Bot):
    """
        Main Class Bot
    """

    def __init__(self):
        super().__init__(command_prefix="k!")
        self.token = CREDS
        self.remove_command('help')

        for extension in EXTENSIONS:
            try:
                self.load_extension(extension)
            except Exception as error:
                print('Failed to load extension {}\n{}: {}'.format(
                    extension, type(error).__name__, error))

    async def setup_default_channel_perms(self, current_server, bot_admin_role):
        """
            Creates the default channel permissions for everyone, kyaru and admins
        """
        everyone_perms = discord.PermissionOverwrite(read_messages=False, send_messages=False)

        kyaru_bot_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True, manage_channels=True, read_message_history=True, add_reactions=True, manage_messages=True, embed_links=True, manage_roles=True, external_emojis=True)

        kyaru_admin_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True, manage_channels=True, manage_messages=True, read_message_history=True, add_reactions=True, external_emojis=True)

        kyaru_role = discord.utils.get(current_server.roles, name=self.user.name)
        kyaru_admin_role = discord.utils.get(current_server.roles, name=bot_admin_role)

        overwrite_perms = {
            current_server.default_role: everyone_perms,
            kyaru_role: kyaru_bot_perms,
            kyaru_admin_role: kyaru_admin_perms
        }

        return overwrite_perms

    async def setup_default_roles_channels(self, current_server):
        """
            Creates the default channels needed for the bot
        """
        bot_admin_role = "Kyaru's Playground"
        if discord.utils.get(current_server.roles, name=bot_admin_role) is None:
            await current_server.create_role(name=bot_admin_role)

        bot_commands_channel = "kyaru-playground"
        if discord.utils.get(current_server.channels, name=bot_commands_channel) is None:

            permissions_dictionary = await self.setup_default_channel_perms(current_server, bot_admin_role)
            await current_server.create_text_channel(bot_commands_channel, overwrites=permissions_dictionary)

        cb_queue_channel = "cb-boss-queues"
        if discord.utils.get(current_server.channels, name=cb_queue_channel) is None:

            permissions_dictionary = await self.setup_default_channel_perms(current_server, bot_admin_role)
            await current_server.create_text_channel(cb_queue_channel, overwrites=permissions_dictionary)

    async def on_guild_join(self, guild):
        """
            Called when bot joins a server
        """
        await self.wait_until_ready()
        await self.setup_default_roles_channels(guild)
        server_queries.insert_servers_entry(int(guild.id), 'False', 'True', "'round'", 'False', "'05:00'")

    async def on_ready(self):
        """
            Function that calls client is done preparing the data received from Discord
        """

        print('Logged in as')
        client_array = setup.get_client_details(self)
        print(client_array[0])
        print('------')
        await self.change_presence(status=discord.Status.online, activity=discord.Game(name='3.0.0'))

        for server in self.guilds:
            await self.setup_default_roles_channels(server)

    async def close(self):
        await super().close()
        # await self.session.close()

    def run(self):
        super().run(self.token, reconnect=True)


if __name__ == '__main__':
    KYARUBOT = KyaruBot()
    KYARUBOT.run()
