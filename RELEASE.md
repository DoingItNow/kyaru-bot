# Kyaru Bot Release Notes

# 3.0.0
* Created a new queuing type on top of `round`. Queues can be done per `day` instead for less granular queues
* Refactored and moved cog functionality around.
* `k!rm` has improved functionality. k!rm now accepts a discord username where it will specify which user to remove from the current boss

# 2.7.3
* Changed how to top up the queue to always topping the queue by 1 when the round is over
* Decraeased the initial rounds from 10 to 5 from the changes above

# 2.7.2
* Added an exception handler to check if a discord handle does not exist in the server but exists in the DB
* Extended `k!rm` to accept a discord username so other uses can remove other people from the queue
* Depracated `k!list_dead` and `k!list_alive` due to lack of usage
* KLUDGE fixing an issue where adding a role to Kyaru is slower than getting Kyaru's role list
    * Temporary it will hardcode wait for 1.2s to HOPEFULLY get all the roles.

# 2.7.1
* Fixed an issue where the order of users in the list was incorrectly ordered
* Increased the amount of roles to creae when there are no roles from 8 to 10
* Increased the amount of rounds/role to create where there are roles from 6 to 8

# 2.7.0
* Added new cog to cater to sending gifs

# 2.6.2
* Increased the amount of roles to create at daily reset from 5 to 8
* Increased the amount of rounds/role to create when there is none left from 2 to 4

# 2.6.1
* Removed getting the list from k!rm

# 2.6.0
* Added k!rm feature

# 2.5.1
* Updated discord.py to 1.2.2 and modified the loop task to be a decorator

# 2.5.0
* Updated the user list for boss queues to include nicknames of a discord user if it exists
* Added k!current_list command

# 2.4.1
* Fixed issue where roles won't get cleaned up at reset time.
    * There is a time period (3 minutes for now) where roles will be cleaned up and role created/queued
* Added an additional buffer of roles (by 2) when all the bosses are dead

# 2.4.0
* Updated `k!set_cb_schedule_timer` to do dates for CB. Old functionality is now called `k!start_cb_schedule_timer` instead.

# 2.3.0
* `k!dead` and `k!revive` have be revamped to use a state table containing the current boss

# 2.2.0
* Updated the list boss queues to have timestamps included

# 2.1.0
* Put reset time in the DB so it can be used for the command k!update_reset_timer

# 2.0.0
* Changed internal bot code Sto conform to discord.py version 1.0.1

# 1.2.0
* Added new feature where round queues will be crossed out when all bosses are dead in that round
* Lots of bug fixes

# 1.1.0
* Using Postgres DB instead of using class instance variables due to Heroku Dynos restarting randomly

# 1.0.0 (Initial Release)
* Did all the things