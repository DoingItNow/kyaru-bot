.PHONY: test phpunit pyunit
CURRENT_DIRECTORY=$(shell pwd)

tests: style_check pyunit

install_packages:
	pip install -r requirements.txt

pyunit:
	PYTHONPATH=$(CURRENT_DIRECTORY)/tests python3 -m unittest tests

style_check:
	python3 -B -m pylint --rcfile=tests/pylintrc  cogs/ config/ functions/ tests/

bot-run:
	python3 main.py
