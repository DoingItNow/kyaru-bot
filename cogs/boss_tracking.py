"""
Cog that does tracks the boss role with dead or revive
"""
import time
import discord
from discord.ext import commands
from functions import get_bot_member
from functions import handle_boss_roles
from functions import automated_mod_roles
from functions import queuing_round
# from functions import queuing_day
from queries import role_queries
from queries import server_queries
from queries import message_queries

class BossTrackingCog(commands.Cog):
    """
        Cog containing commands for reaction things
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def dead(self, ctx): # pylint: disable=R0914
        """
            Set the boss role as dead to be cleaned by adding the bot in the role
        """
        message = ctx.message
        server = message.guild
        members = server.members
        bot_user = self.bot.user

        bot_member = get_bot_member.get_bot_as_member(bot_user, members)
        queue_type = server_queries.get_server_attribute('queue_type', int(server.id))

        current_boss = role_queries.get_role_attribute(server.id, 'boss_role')

        boss_role_split_list = current_boss.split("-")

        new_boss_role = automated_mod_roles.next_boss_role(boss_role_split_list)
        new_boss_role_quotes = "'%s'" % new_boss_role
        new_boss_role_split = new_boss_role.split("-")

        role_queries.update_role_entry('boss_role', new_boss_role_quotes, server.id)
        print("The current role has been updated to " + new_boss_role)

        if queue_type == "day":
            current_day_number = role_queries.get_role_attribute(int(server.id), 'day_number')
            new_boss_name = "day-" + str(current_day_number) + "-boss-" + new_boss_role_split[3]
            new_boss_role = discord.utils.get(server.roles, name=new_boss_name)
            death_message = "Boss " + str(boss_role_split_list[3]) + " has been marked as dead.\n" \
                "The role will be cleaned up at server reset.\n\nThe current boss that is alive is:** " +  \
                current_boss + "**.\nPinging next boss: " + new_boss_role.mention
        else:
            dead_boss_role = discord.utils.get(server.roles, name=current_boss)

            await bot_member.add_roles(dead_boss_role)

            # KLUDGE: There is a race condition where getting the bot's role occurs before the next role is addded
            # Set a sleep to 1 to temp fix this
            print("Sad waiting for the role to be added to Kyaru")
            time.sleep(1.2)

            # Check the dead bosses to find out of the round is completely done so
            # the edit of the messages can be done
            bot_boss_roles = automated_mod_roles.get_bot_boss_roles(ctx.bot, server.members, queue_type)
            dead_rounds = automated_mod_roles.get_dead_boss_rounds(bot_boss_roles)
            print("The dead rounds to be editted is", dead_rounds)
            await queuing_round.edit_dead_boss_messages(server, dead_rounds)

            current_boss = discord.utils.get(server.roles, name=new_boss_role)
            # Check if the next role exists in the server, if not create the roles and add it to the cb-boss-queue channel
            if int(boss_role_split_list[3]) == 5:
                message_id = message_queries.get_queue_message_id(server.id, int(new_boss_role_split[1])+2)
                if message_id is None:

                    # Use for testing
                    # await handle_boss_roles.create_boss_roles(server, queue_type, int(new_boss_role_split[1])+1, int(new_boss_role_split[1])+1)
                    # await queuing_round.create_next_messages(bot_user, server, (int(new_boss_role_split[1])+1, int(new_boss_role_split[1])+1), queue_type)

                    await handle_boss_roles.create_boss_roles(server, queue_type, int(new_boss_role_split[1])+2, int(new_boss_role_split[1])+2)
                    await queuing_round.create_next_messages(bot_user, server, (int(new_boss_role_split[1])+2, int(new_boss_role_split[1])+2), queue_type)
                else:
                    print("Message for the queue has been created: Skipping")

            death_message = "Round " + str(boss_role_split_list[1]) + " Boss " + str(boss_role_split_list[3]) + " has been marked as dead.\n" \
                "The role will be cleaned up at server reset.\n\nThe current boss that is alive is: " +  \
                current_boss.mention
        await ctx.send(death_message)

    @commands.command(pass_context=True)
    async def revive(self, ctx):
        """
            Set the boss as alive where the bot is removed form the role
        """
        message = ctx.message
        server = message.guild
        members = server.members
        bot_user = self.bot.user

        bot_member = get_bot_member.get_bot_as_member(bot_user, members)
        queue_type = server_queries.get_server_attribute('queue_type', int(server.id))

        current_boss = role_queries.get_role_attribute(server.id, 'boss_role')
        boss_role_split_list = current_boss.split("-")

        # Get the previous round-boss role
        new_boss_role = automated_mod_roles.previous_boss_role(boss_role_split_list)
        new_boss_role_quotes = "'%s'" % new_boss_role
        role_queries.update_role_entry('boss_role', new_boss_role_quotes, server.id)

        revived_boss_role_split = new_boss_role.split("-")

        if queue_type == "round":
            revived_boss_role = discord.utils.get(server.roles, name=new_boss_role)
            await bot_member.remove_roles(revived_boss_role)

        alive_message = queue_type.capitalize() +  " " + str(revived_boss_role_split[1]) + " Boss " + str(
            revived_boss_role_split[3]) + " has been marked as still alive."
        await ctx.send(alive_message)

def setup(bot):
    """
        Setups the cog to be used for the bot
    """
    bot.add_cog(BossTrackingCog(bot))
