"""
Cog that will update channel permissions for kyaru and CB members
"""
import discord
from discord.ext import commands


class ChannelSetup(commands.Cog):
    """
    Cog that will update channel permissions for kyaru and CB members
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def set_cb_boss_queue_perms(self, ctx, *, role_name: str):
        """
            Function that calls to get the help message for the user
        """
        member = ctx.message.author
        message = ctx.message
        server = message.guild

        admin_role = discord.utils.get(server.roles, name="Kyaru's Playground")
        cb_member_role = discord.utils.get(server.roles, name=role_name)
        cb_boss_queue_channel = discord.utils.get(server.channels, name="cb-boss-queues")

        if admin_role in member.roles and message.channel.name == "kyaru-playground":
            await cb_boss_queue_channel.set_permissions(cb_member_role, read_messages=True, send_messages=False, read_message_history=True, add_reactions=False)
            await ctx.send(role_name + " has channel permissions updated for cb-boss-queues")


def setup(bot):
    """
        Setups the cog to be used for the bot
    """
    bot.add_cog(ChannelSetup(bot))
