"""
Cog that schedules creating/deleting of queues and roles
"""
from datetime import date
from datetime import datetime

import discord
from discord.ext import commands
from discord.ext import tasks

from functions import automated_mod_roles
from functions import handle_boss_roles
from functions import get_bot_member
from functions import queuing_round
from functions import queuing_day

from queries import message_queries
from queries import server_queries
from queries import boss_queries
from queries import role_queries

class SchedulerCog(commands.Cog):
    """
        Cog containing commands for creates, deletes and lists boss roles
    """

    def __init__(self, bot):
        self.bot = bot
        self.cb_scheduler.start()  # pylint: disable=no-member

    def set_servers_default_vars(self):
        """
            Sets the default instance variables for the servers the bot is in
        """
        print("Setting up default vars")
        for server in self.bot.guilds:
            server_queries.insert_servers_entry(int(server.id), 'False', 'True', "'round'", 'False', "'05:00'")

    @tasks.loop(seconds=60.0)
    async def cb_scheduler(self): # pylint: disable=R0915
        """
            Goes through each of the servers and checks if the roles needed to be updated
        """
        bot_servers = self.bot.guilds
        bot_user = self.bot.user

        for server in bot_servers:
            schedule_enabled = self.get_scheduled_status(server)
            queue_type = server_queries.get_server_attribute('queue_type', int(server.id))

            if schedule_enabled == 'true':
                print("Running CB Scheduler for " + server.name)

                cb_reset_time = message_queries.get_reset_time(int(server.id))
                current_time = datetime.strftime(datetime.now(), '%H:%M')
                print("The current time is: " + str(current_time))
                print("The reset time is: " + str(cb_reset_time))

                current_time_int = get_bot_member.convert_time(current_time)
                cb_reset_time_int = get_bot_member.convert_time(cb_reset_time)
                print("The current time in an integer is: " + str(current_time_int))
                print("The reset time threshold in an integer is: Min:" + str(cb_reset_time_int-2) + " Max: " + str(cb_reset_time_int+2))

                if cb_reset_time_int - 2 <= current_time_int <= cb_reset_time_int + 2:
                    await self.schedule_boss_roles(server, queue_type, bot_user)
                else:
                    print("Server is not at reset time for " + server.name + "\n")
                    if queue_type == "day":
                        # Add the day roles into Kyaru once the check to cleanup roles is done
                        await queuing_day.add_day_boss_roles(self.bot, server, queue_type)
            else:
                print("CB Scheduler is not set for: " + server.name + "\n")
                post_delete = server_queries.get_server_attribute('post_delete', int(server.id))

                if post_delete == 'true':
                    print("Post CB Scheduling has been set. Cleaning up roles and message queues")
                    await handle_boss_roles.delete_boss_roles(server, queue_type)

                    message_queries.delete_messages_from_server(int(server.id))
                    boss_queries.delete_queue_reset(server.id)
                    server_queries.update_server_attribute('post_delete', 'False', int(server.id))
                    print("Post cleanup has been completed!\n")


    @cb_scheduler.before_loop
    async def before_cb_scheduler(self):
        """
            Before Loop Function to wait for bot and get default vars
        """
        print('Waiting for bot to load')
        await self.bot.wait_until_ready()
        self.set_servers_default_vars()

    async def schedule_boss_roles(self, server, queue_type, bot_user):
        """
            Schedule the next boss roles to be created for the day/round.

            Creates a new set of roles if they have not been created before.
            Deletes old roles and creates new ones if it is a new day
            Deletes old roles that are dead for rounds
        """
        queue_setup = server_queries.get_server_attribute('queue_setup', int(server.id))
        if queue_setup == 'true':
            await self.initialise_queues(server, queue_type)

            if queue_type == "day":
                # Creates the boss roles and sends the message to cb-boss-queues
                # Add the day roles to Kyaru
                await handle_boss_roles.create_boss_roles(server, queue_type, 1, 1)
                await queuing_day.create_next_messages(bot_user, server, 1, queue_type)
            else:
                await handle_boss_roles.create_boss_roles(server, queue_type, 1, 3)
                await queuing_round.create_next_messages(bot_user, server, (1, 3), queue_type)

            server_queries.update_server_attribute('queue_setup', 'False', int(server.id))
            print("First time Queue Setup has been completed. Setting setup to false\n")
        else:
            # Normal Scenario - Determin cleanup/creation of roles
            bot_boss_roles = automated_mod_roles.get_bot_boss_roles(self.bot, server.members, queue_type)

            if queue_type == "day":
                # If there is boss roles in Kyaru, delete them and increment a new day
                if bot_boss_roles:
                    await queuing_round.delete_dead_boss_roles(server, bot_boss_roles)

                    # Get the current day and increase by one. Create the roles and send the message
                    current_day_number = role_queries.get_role_attribute(int(server.id), 'day_number')
                    next_day_number = int(current_day_number) + 1

                    await handle_boss_roles.create_boss_roles(server, queue_type, next_day_number, next_day_number)
                    await queuing_day.create_next_messages(bot_user, server, next_day_number, queue_type)

                    role_queries.update_role_entry('day_number', next_day_number, server.id)

                    await queuing_day.edit_dead_boss_messages(server, int(current_day_number))
                else:
                    print("Boss Roles have been created for today. Skip creating day roles\n")
            else:
                await queuing_round.delete_dead_boss_roles(server, bot_boss_roles)
                dead_rounds = automated_mod_roles.get_dead_boss_rounds(bot_boss_roles)
                print(dead_rounds)

                await queuing_round.edit_dead_boss_messages(server, dead_rounds)

    @staticmethod
    def get_scheduled_status(server):
        """
            Gets the scheduled status based of a start and end time if applicable
        """
        current_schedule_enabled = server_queries.get_server_attribute('schedule_enabled', server.id)
        start_date = str(server_queries.get_server_attribute('start_date', server.id))
        end_date = str(server_queries.get_server_attribute('end_date', server.id))
        current_date = str(datetime.strftime(date.today(), '%Y-%m-%d'))

        print("The current date is: " + current_date)
        print("The start date for CB is: " + start_date)
        print("The end date for CB is: " + end_date)

        if start_date == current_date:
            print("Start Date is today: Checking scheduling state")
            if current_schedule_enabled == "false":
                server_queries.update_server_attribute('schedule_enabled', 'True', server.id)
                print("Set scheduling timer to true\n")
            schedule_enabled = server_queries.get_server_attribute('schedule_enabled', server.id)
        elif end_date == current_date:
            print("End Date is today: Checking scheduling state")
            if current_schedule_enabled == "true":
                server_queries.update_server_attribute('schedule_enabled', 'False', server.id)
                server_queries.update_server_attribute('post_delete', 'True', server.id)
                server_queries.update_server_attribute('queue_setup', 'True', int(server.id))

                server_queries.update_server_attribute('start_date', "NULL", server.id)
                server_queries.update_server_attribute('end_date', "NULL", server.id)
                print("Resetting Start/End dates")

                role_queries.delete_role_entry(server.id)
                print("Set scheduling timer to false\n")
            schedule_enabled = server_queries.get_server_attribute('schedule_enabled', server.id)
        else:
            print("Today is not the start or the end of CB for: " + server.name + "\n")
            schedule_enabled = server_queries.get_server_attribute('schedule_enabled', server.id)

        return schedule_enabled

    @staticmethod
    async def initialise_queues(server, queue_type):
        """
            Sets the initial boss role in the DB
        """

        current_date = datetime.strftime(date.today(), '%d-%m-%Y')
        initial_boss_role = "'%s'" % "round-1-boss-1"

        if queue_type == "day":
            role_queries.insert_role_entry(initial_boss_role, server.id, 1)
        else:
            role_queries.insert_role_entry(initial_boss_role, server.id, "NULL")

        queue_channel = discord.utils.get(server.channels, name="cb-boss-queues")
        await queue_channel.send("**Clan Battle Boss Queues for: " + current_date + "**")

    @commands.command(pass_context=True)
    async def set_cb_schedule_timer(self, ctx, start_date: str, end_date: str):
        """
            Sets the CB Schedule timer to be on
        """
        message = ctx.message
        member = ctx.message.author

        server = message.guild
        current_channel = ctx.message.channel.name

        admin_role = discord.utils.get(server.roles, name="Kyaru's Playground")

        if admin_role in member.roles and current_channel == "kyaru-playground":
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            end_date = datetime.strptime(end_date, '%Y-%m-%d')

            start_date = "'%s'" % start_date
            end_date = "'%s'" % end_date

            server_queries.update_server_attribute('start_date', start_date, server.id)
            server_queries.update_server_attribute('end_date', end_date, server.id)
            await ctx.send("CB start and end dates have been set!")

    @commands.command(pass_context=True)
    async def start_cb_schedule_timer(self, ctx):
        """
            Sets the CB Schedule timer to be on
        """
        message = ctx.message
        member = ctx.message.author

        server = message.guild
        current_channel = ctx.message.channel.name

        admin_role = discord.utils.get(server.roles, name="Kyaru's Playground")

        if admin_role in member.roles and current_channel == "kyaru-playground":
            server_queries.update_server_attribute('schedule_enabled', 'True', server.id)
            await ctx.send("CB Scheduling has been set! It will be updated at server reset.")

    @commands.command(pass_context=True)
    async def stop_cb_schedule_timer(self, ctx):
        """
            Stops the CB Schedule timer and set to delete/clean up all boss roles
        """
        message = ctx.message
        member = ctx.message.author

        server = message.guild
        current_channel = ctx.message.channel.name

        admin_role = discord.utils.get(server.roles, name="Kyaru's Playground")

        if admin_role in member.roles and current_channel == "kyaru-playground":
            server_queries.update_server_attribute('schedule_enabled', 'False', server.id)
            server_queries.update_server_attribute('post_delete', 'True', server.id)
            server_queries.update_server_attribute('queue_setup', 'True', server.id)

            role_queries.delete_role_entry(server.id)

            await ctx.send("CB Scheduling has been stopped!")

    @commands.command(pass_context=True)
    async def update_reset_timer(self, ctx, reset_time: str):
        """
            Stops the CB Schedule timer and set to delete/clean up all boss roles
        """
        message = ctx.message
        member = ctx.message.author

        server = message.guild
        current_channel = ctx.message.channel.name

        # PUT TIME IN QUOTES
        reset_time = "'%s'" % reset_time

        admin_role = discord.utils.get(server.roles, name="Kyaru's Playground")

        if admin_role in member.roles and current_channel == "kyaru-playground":
            server_queries.update_server_attribute('reset_time', reset_time, server.id)

            await ctx.send("Reset time has been changed!")

    @commands.command(pass_context=True)
    async def update_queue_type(self, ctx, queue_type: str):
        """
            Changes to queue type
        """
        message = ctx.message
        member = ctx.message.author

        server = message.guild
        current_channel = ctx.message.channel.name

        admin_role = discord.utils.get(server.roles, name="Kyaru's Playground")
        schedule_enabled = server_queries.get_server_attribute('schedule_enabled', server.id)

        if schedule_enabled != "true":
            if queue_type in ('day', 'round'):
                if admin_role in member.roles and current_channel == "kyaru-playground":
                    queue_type_quotes = "'%s'" % queue_type
                    server_queries.update_server_attribute('queue_type', queue_type_quotes, server.id)

                    await ctx.send("Queue type has been changed to " + queue_type)
            else:
                await ctx.send("The queue type sent is invalid. Valid types is `day` or `round`")
        else:
            await ctx.send("You cannot set the queue type when CB has started")

def setup(bot):
    """
        Setups the cog to be used for the bot
    """
    bot.add_cog(SchedulerCog(bot))
