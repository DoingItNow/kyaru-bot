"""
Cog that sends gifs
"""
import discord
from discord.ext import commands

class GifMessagesCog(commands.Cog):
    """
        Cog containing commands for creates, deletes and lists boss roles
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def suicide(self, ctx):
        """
            Command that sends an Ilya ded gif
        """
        file = discord.File("gifs/kdead.gif", filename="dedilya.gif")
        await ctx.send(file=file)

def setup(bot):
    """
        Setups the cog to be used for the bot
    """
    bot.add_cog(GifMessagesCog(bot))
