"""
Cog that creates, deletes and lists boss roles
"""
import discord
from discord.ext import commands
from functions import get_bot_member

from queries import boss_queries
from queries import role_queries
from queries import message_queries
from queries import server_queries


class BossQueueCog(commands.Cog):
    """
        Cog containing commands for creates, deletes and lists boss roles
    """

    def __init__(self, bot):
        self.bot = bot

    @staticmethod
    def create_list_message(server, boss_role, bot_username, queue_type):
        """
            Creates a message which lists out which members have a particular boss role
        """
        server_id = server.id

        boss_role_split_list = boss_role.name.split("-")

        queue_number = boss_role_split_list[1]
        boss_number = boss_role_split_list[3]
        boss_name = "boss" + boss_number

        user_queue_list = []

        db_user_queue = boss_queries.get_users_from_boss(boss_name, server_id, int(queue_number))

        if not db_user_queue:
            ping_message = "**" + queue_type.capitalize() + " " + queue_number + " Boss " + boss_number + \
                " Queue**\nNo one currently has queued for this boss.\n"
        elif bot_username in boss_role.members and queue_type != "day":
            ping_message = "**" + queue_type.capitalize() + " " + queue_number + " Boss " + boss_number + \
                " Queue**\nBoss is current marked as dead.\n"
        else:
            try:
                for user_entry in db_user_queue:
                    discord_name_list = user_entry[0].split("#")
                    discord_member = discord.utils.get(server.members, name=discord_name_list[0], discriminator=str(discord_name_list[1]))

                    if discord_member.nick is None:
                        user_with_timestamp = user_entry[0] + " at " + user_entry[1] + " JST"
                    else:
                        user_with_timestamp = discord_member.nick + " at " + user_entry[1] + " JST"

                    user_queue_list.append(user_with_timestamp)

                ping_message = "**" + queue_type.capitalize() + " " + queue_number + " Boss " + boss_number + " Queue**\nThe following people queued for boss " + boss_number + ":\n" \
                    "```\n" + "\n".join(user_queue_list) + "```"
            except AttributeError:
                ping_message = "**ヤバい**\nA discord user does not exist in the server or someone has changed their discord handle\n" \
                               "The list cannot be shown."
        return ping_message

    @commands.command()
    async def list(self, ctx, boss_role_name: str):
        """
            Command that lists out a particular boss role
        """

        # Set the context
        message = ctx.message
        server = message.guild
        members = server.members

        bot_member = get_bot_member.get_bot_as_member(ctx.bot.user, members)

        boss_role = discord.utils.get(server.roles, name=boss_role_name)

        queue_type = server_queries.get_server_attribute('queue_type', int(server.id))

        ping_message = self.create_list_message(server, boss_role, bot_member, queue_type)
        await ctx.send(ping_message)

    @commands.command()
    async def current_list(self, ctx):
        """
            Command to gets the current list of people hitting the current boss
        """

        # Set the context
        message = ctx.message
        server = message.guild
        members = server.members

        queue_type = server_queries.get_server_attribute('queue_type', int(server.id))
        current_boss = role_queries.get_role_attribute(server.id, 'boss_role')

        if queue_type == "day":
            current_boss_split_list = current_boss.split("-")
            current_day_number = role_queries.get_role_attribute(int(server.id), 'day_number')

            current_boss = "day-" + str(current_day_number) + "-boss-" + current_boss_split_list[3]

        bot_member = get_bot_member.get_bot_as_member(ctx.bot.user, members)

        boss_role = discord.utils.get(server.roles, name=current_boss)

        ping_message = self.create_list_message(server, boss_role, bot_member, queue_type)
        await ctx.send(ping_message)

    @commands.command()
    async def rm(self, ctx, discord_username=None): # pylint: disable=C0103
        """
            Command that removes a user from the current boss queue
        """

        # Set the context
        message = ctx.message
        server = message.guild
        member = ctx.message.author

        # If there is no discord_username use itself
        if discord_username is None:
            discord_username = member.name

        user_to_delete = discord.utils.get(message.guild.members, display_name=discord_username)

        queue_type = server_queries.get_server_attribute('queue_type', int(server.id))

        current_boss = role_queries.get_role_attribute(server.id, 'boss_role')
        current_boss_split_list = current_boss.split("-")

        queue_number = current_boss_split_list[1]
        boss_number = current_boss_split_list[3]

        if queue_type == "day":
            queue_number = role_queries.get_role_attribute(int(server.id), 'day_number')
            current_boss = "day-" + str(queue_number) + "-boss-" + boss_number

        boss_role = discord.utils.get(server.roles, name=current_boss)
        try:
            # Check that the user has the current role
            if boss_role in user_to_delete.roles:
                print("User to delete " + user_to_delete.name)

                # Remove the reaction from the message as that user
                round_message_id = message_queries.get_queue_message_id(server.id, queue_number)
                queue_channel = discord.utils.get(server.channels, name="cb-boss-queues")
                round_message = await queue_channel.fetch_message(round_message_id)
                await round_message.remove_reaction(boss_number + '\N{COMBINING ENCLOSING KEYCAP}', user_to_delete)

                await ctx.send("The current boss role has been removed from " + discord_username)
            else:
                await ctx.send(discord_username + " is not currently queued for that boss")
        except AttributeError:
            await ctx.send("Discord Username does not exist in the server")

    @commands.command()
    async def current(self, ctx):
        """
            Command to gets the current boss thats alive
        """

        # Set the context
        message = ctx.message
        server = message.guild

        current_boss = role_queries.get_role_attribute(server.id, 'boss_role')
        await ctx.send("The current boss that is alive is:** " + current_boss + "**")

    @commands.Cog.listener()
    async def on_message(self, message):
        """
            Called when message is sent
        """
        mentioned_roles = message.role_mentions
        server = message.guild
        members = server.members

        bot_member = get_bot_member.get_bot_as_member(self.bot.user, members)
        queue_type = server_queries.get_server_attribute('queue_type', int(server.id))

        for role in mentioned_roles:
            if queue_type in role.name and "-boss-" in role.name:
                ping_message = self.create_list_message(server, role, bot_member, queue_type)
                await message.channel.send(ping_message)

def setup(bot):
    """
        Setups the cog to be used for the bot
    """
    bot.add_cog(BossQueueCog(bot))
