"""
Cog that sends help messages for admin and normal users
"""
import discord
from discord.ext import commands


class HelpCog(commands.Cog):
    """
    Cog that sends help messages for admin and normal users
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def help(self, ctx):
        """
            Function that calls to get the help message for the user
        """
        await self.help_response_normal(ctx)

    async def help_response_normal(self, ctx):
        """
            Message that is sent for normal users
        """
        embedded_message = discord.Embed(colour=0x2A2A3D)
        embedded_message.title = ""
        field_name = "Kyaru Bot Commands"
        field_value = ('**Command List**:\n'
                       '```k!list [boss-role] - Lists who has queued for a particular boss in a particular round.\n'
                       'Example: k!list round-1-boss-1\n\n'
                       'k!current - Gets the current boss that is currently alive\n\n'
                       'k!current_list - Gets the people who queued for the current boss\n\n'
                       'k!rm [discord username]- Removes a user from the current boss queue.\n\n'
                       'If the discord username is empty, it will remove yourself. Note discord username is the name that is being shown in discord\n\n'
                       'Example: k!rm TestingUser will remove TestingUser\n'
                       'Example: k!rm will remove yourself\n\n'
                       'k!dead - Marks the boss as dead so the role is cleaned up at reset time\n\n'
                       'k!revive - Marks the boss as alive in case the boss was accidentally marked as dead```\n\n'
                       'Pinging a boss role (example `@round-1-boss-1`) will ping the people who have the role and also list '
                       'who has queued for that boss.\n\n**NOTE**: If you want to only see who has queued for a boss use `k!list` instead.')

        embedded_message.add_field(name=field_name, value=field_value)
        await ctx.send(embed=embedded_message)

    async def help_response_admin(self, ctx, message, member):
        """
            Message that is sent for admins users
        """
        admin_role = discord.utils.get(message.guild.roles, name="Kyaru's Playground")
        if admin_role in member.roles and message.channel.name == "kyaru-playground":
            normal_admin_message = discord.Embed(colour=0x2A2A3D)
            normal_admin_message.title = ""
            field_name = "Kyaru Bot Admin Commands"
            field_value = ('**Command List**:\n'
                           '```k!start_cb_schedule_timer - Sets the CB Schedule timer on. Roles will be checked at reset time 5:00 JST\n\n'
                           'k!stop_cb_schedule_timer - Stops the CB Scheudle timer. Roles will be cleaned up\n\n'
                           'k!set_cb_schedule_timer [start date: YYYY-MM-DD] [end date: YYYY-MM-DD] - set the dates when CB starts\n\n'
                           'Example: k!set_cb_schedule_timer 2019-01-23 2019-01-31\n\n'
                           'k!update_queue_type [day or round]: Change the queue type to day or round. Cannot be set when CB starts\n'
                           'Example: k!update_queue_type day\n\n'
                           'k!set_cb_boss_queue_perms [member_role] - set a specific role to have read access to cb-boss-queues\n'
                           'Example: k!set_cb_boss_queue_perms my-clan-members`\n\n'
                           'k!update_reset_timer [24hr time] - Changes the server reset time\n'
                           'Example: k!update_reset_time 04:32```\n'
                           '**Note**: Either use `k!start_cb_schedule_timer` or `k!set_cb_schedule_timer` but not both. Unintended results may occur.\n\n'
                           )

            normal_admin_message.add_field(name=field_name, value=field_value)
            await ctx.send(embed=normal_admin_message)

    @commands.command()
    async def help_admin(self, ctx):
        """
            Function that calls to get the help message for the admin
        """
        member = ctx.message.author
        message = ctx.message
        await self.help_response_admin(ctx, message, member)


def setup(bot):
    """
        Setups the cog to be used for the bot
    """
    bot.add_cog(HelpCog(bot))
