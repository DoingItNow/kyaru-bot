"""
Cog that does reactions for boss queues
"""
from discord.ext import commands
from functions import get_bot_member
from functions import handle_boss_roles

from queries import message_queries
from queries import server_queries

class ReactionsCog(commands.Cog):
    """
        Cog containing commands for reaction things
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        """
            Function that calls bot when a reaction is added to a message
        """
        reaction_message_id = payload.message_id
        reaction_channel = self.bot.get_channel(payload.channel_id)

        message = await reaction_channel.fetch_message(payload.message_id)

        server = message.guild
        members = server.members

        boss_emoji = payload.emoji
        user = self.bot.get_user(payload.user_id)
        bot_id = self.bot.user.id

        member = get_bot_member.get_bot_as_member(user, members)

        db_message_id = message_queries.get_react_message_id(int(reaction_message_id))
        if db_message_id is not None and not payload.user_id == bot_id:
            queue_type = server_queries.get_server_attribute('queue_type', int(server.id))
            await handle_boss_roles.assign_boss_role(member, message, boss_emoji.name, queue_type)
        elif payload.user_id == bot_id:
            print("Kyaru sent a reaction but did not add the role to herself")
        else:
            # Note that this will trigger everytime a reaction is added when Kyaru is looking at a channel
            # Regardless if it is a CB reaction or not
            print("Message ID not found in DB")

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        """
            Function that calls bot when a reaction is removed to a message
        """
        reaction_message_id = payload.message_id
        reaction_channel = self.bot.get_channel(payload.channel_id)

        message = await reaction_channel.fetch_message(payload.message_id)
        server = message.guild
        members = server.members

        boss_emoji = payload.emoji
        user = self.bot.get_user(payload.user_id)
        bot_id = self.bot.user.id

        member = get_bot_member.get_bot_as_member(user, members)
        db_message_id = message_queries.get_react_message_id(int(reaction_message_id))
        if db_message_id is not None and not payload.user_id == bot_id:
            queue_type = server_queries.get_server_attribute('queue_type', int(server.id))
            await handle_boss_roles.remove_boss_role(member, message, boss_emoji.name, queue_type)
        else:
            # Note that this will trigger everytime a reaction is removed when Kyaru is looking at a channel
            # Regardless if it is a CB reaction or not
            print("Message ID not found in DB or Kyaru created the message")

def setup(bot):
    """
        Setups the cog to be used for the bot
    """
    bot.add_cog(ReactionsCog(bot))
