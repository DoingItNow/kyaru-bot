"""
File containing functions related to the automating of boss role creation/deletion
and created round message reactions
"""

from functions import get_bot_member


def get_alive_bosses(server_boss_roles, bot_boss_roles):
    """
        Symmetric difference between two list of roles
    """
    server_roles_set = set(server_boss_roles)
    bot_roles_set = set(bot_boss_roles)
    alive_boss_roles = list(server_roles_set.symmetric_difference(bot_roles_set))
    return alive_boss_roles


def get_bot_boss_roles(bot, members, queue_type):
    """
        Gets the boss roles from a bot

        Returns a list of dead_boss_list
    """
    bot_user = bot.user
    dead_boss_list = []

    bot_member = get_bot_member.get_bot_as_member(bot_user, members)
    for role in bot_member.roles:
        if queue_type in role.name and "-boss-" in role.name:
            dead_boss_list.append(role)
    return dead_boss_list


def get_server_boss_roles(server, queue_type):
    """
        Gets the boss roles from a server

        Returns a list of boss roles in the server
    """
    server_boss_list = []

    for role in server.roles:
        if queue_type in role.name and "-boss-" in role.name:
            server_boss_list.append(role)
    return server_boss_list


def get_next_round_numbers(alive_boss_roles, bot_boss_roles):
    """
        Determines the next rounds to be created based of the
        currently alive bosses and the dead bosses
    """
    if alive_boss_roles:
        alive_boss_round_numbers = []

        for role in alive_boss_roles:
            role_name_split = role.name.split("-")
            alive_boss_round_numbers.append(int(role_name_split[1]))

        alive_lowest_number = min(alive_boss_round_numbers)
        alive_highest_number = max(alive_boss_round_numbers)

        print(alive_boss_round_numbers)
        print("Highest Boss Round is " + str(alive_highest_number))
        print("Lowest Boss Round is " + str(alive_lowest_number))

        highest_round_number = alive_lowest_number + 2
        if highest_round_number > alive_highest_number:
            lowest_round_number = alive_highest_number + 1
            return (lowest_round_number, highest_round_number)
        return None

    if bot_boss_roles:
        bot_boss_round_numbers = []

        for role in bot_boss_roles:
            role_name_split = role.name.split("-")
            bot_boss_round_numbers.append(role_name_split[1])
        bot_boss_round_numbers.sort()

        bot_highest_number = bot_boss_round_numbers[len(bot_boss_round_numbers)-1]
        lowest_round_number = int(bot_highest_number) + 1
        highest_round_number = int(bot_highest_number) + 2
        return (lowest_round_number, highest_round_number)

    return (1, 2)


def get_dead_boss_rounds(bot_boss_roles):
    """
        Determines the if a round is consider dead based of
        how many boss roles are said to be dead
    """
    if bot_boss_roles:
        print(bot_boss_roles)
        round_and_boss_numbers = []

        # Get a tuple of a round and its associated boss
        for role in bot_boss_roles:
            role_name_split = role.name.split("-")
            round_and_boss_numbers.append((int(role_name_split[1]), int(role_name_split[3])))

        # If the boss number is 5, add the round to the dead_boss_rounds list
        dead_boss_rounds = []
        for round_tuple in round_and_boss_numbers:
            if round_tuple[1] == 5:
                dead_boss_rounds.append(round_tuple[0])

        return dead_boss_rounds
    return None


def next_boss_role(role_split):
    """
        Gets the next boss role that is set as current
    """
    round_number = int(role_split[1])
    boss_number = int(role_split[3])

    boss_number = boss_number + 1

    if boss_number > 5:
        boss_number = 1
        round_number = round_number + 1

    new_role_name = "round-" + str(round_number) + "-boss-" + str(boss_number)
    return new_role_name


def previous_boss_role(role_split):
    """
        Gets the previous boss role that was marked as dead
    """
    round_number = int(role_split[1])
    boss_number = int(role_split[3])

    boss_number = boss_number - 1

    if boss_number < 1:
        boss_number = 5
        round_number = round_number - 1

    new_role_name = "round-" + str(round_number) + "-boss-" + str(boss_number)
    return new_role_name
