"""
Common Functions used in creating or deleting roles
"""
import discord
from queries import boss_queries

async def create_boss_roles(server, queue_type, lower_bound, upper_bound):
    """
        Creates the boss roles from a range based of the queue type

        Queue type is either round or day
    """
    for queue_number in range(lower_bound, upper_bound+1):
        # Loop thru boss 1 to 5
        for boss_number in range(1, 6):
            boss_role_name = queue_type + "-" + \
                str(queue_number) + "-boss-" + str(boss_number)
            if discord.utils.get(server.roles, name=boss_role_name) is None:
                await server.create_role(name=boss_role_name, mentionable=True)
                print("Created the role " + boss_role_name)


async def delete_boss_roles(server, queue_type):
    """
        Deletes all the boss roles based of the queue type

        Queue type is either round or day
    """
    boss_role_list = []
    for role_class in server.roles:
        if queue_type in role_class.name and "boss" in role_class.name:
            boss_role_list.append(role_class.name)

    if boss_role_list:
        for boss_role in boss_role_list:
            deleted_role = discord.utils.get(server.roles, name=boss_role)
            print("Deleting the role " + boss_role)
            await deleted_role.delete()
    else:
        print("Boss roles do not exist for " + server.name)


async def assign_boss_role(member, message, emoji, queue_type):
    """
        Assigns the boss role based of an emoji

        message.content is "Round/Day [Number] Click the emoji below
        to tag and reserve a spot for a particular boss"
    """
    content = message.content
    content_list = content.split()
    server = message.guild
    discord_username = member.name + "#" + member.discriminator
    discord_username = "'%s'" % discord_username

    queue_type_quotes = "'%s'" % queue_type

    boss_round = content_list[1]

    if emoji == '1\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-1"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Adding it to " + member.name)

        await member.add_roles(boss_role)
        print("Added " + boss_role_name + " to the user " + member.name)
        boss_queries.insert_user_entry("boss1", server.id, discord_username, int(boss_round), queue_type_quotes)

    elif emoji == '2\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-2"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Adding it to " + member.name)

        await member.add_roles(boss_role)
        print("Added " + boss_role_name + " to the user " + member.name)
        boss_queries.insert_user_entry("boss2", server.id, discord_username, int(boss_round), queue_type_quotes)

    elif emoji == '3\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-3"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Adding it to " + member.name)

        await member.add_roles(boss_role)
        print("Added " + boss_role_name + " to the user " + member.name)
        boss_queries.insert_user_entry("boss3", server.id, discord_username, int(boss_round), queue_type_quotes)

    elif emoji == '4\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-4"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Adding it to " + member.name)

        await member.add_roles(boss_role)
        print("Added " + boss_role_name + " to the user " + member.name)
        boss_queries.insert_user_entry("boss4", server.id, discord_username, int(boss_round), queue_type_quotes)

    elif emoji == '5\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-5"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Adding it to " + member.name)

        await member.add_roles(boss_role)
        print("Added " + boss_role_name + " to the user " + member.name)
        boss_queries.insert_user_entry("boss5", server.id, discord_username, int(boss_round), queue_type_quotes)

    else:
        print("Foreign Emoji has been added to the message - ignoring")

async def remove_boss_role(member, message, emoji, queue_type):
    """
        Removes the boss role based of an emoji
    """
    content = message.content
    content_list = content.split()
    server = message.guild
    discord_username = member.name + "#" + member.discriminator
    discord_username = "'%s'" % discord_username

    boss_round = content_list[1]

    if emoji == '1\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-1"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Removing it from " + member.name)

        await member.remove_roles(boss_role)
        print("Removed " + boss_role_name + " from the user " + member.name)
        boss_queries.delete_user_queue("boss1", server.id, discord_username, int(boss_round))

    elif emoji == '2\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-2"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Removing it from " + member.name)

        await member.remove_roles(boss_role)
        print("Removed " + boss_role_name + " from the user " + member.name)
        boss_queries.delete_user_queue("boss2", server.id, discord_username, int(boss_round))

    elif emoji == '3\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-3"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Removing it from " + member.name)

        await member.remove_roles(boss_role)
        print("Removed " + boss_role_name + " from the user " + member.name)
        boss_queries.delete_user_queue("boss3", server.id, discord_username, int(boss_round))

    elif emoji == '4\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-4"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Removing it from " + member.name)

        await member.remove_roles(boss_role)
        print("Removed " + boss_role_name + " from the user " + member.name)
        boss_queries.delete_user_queue("boss4", server.id, discord_username, int(boss_round))

    elif emoji == '5\N{COMBINING ENCLOSING KEYCAP}':
        boss_role_name = queue_type + "-" + boss_round + "-boss-5"
        boss_role = discord.utils.get(server.roles, name=boss_role_name)
        print("Found role. Removing it from " + member.name)

        await member.remove_roles(boss_role)
        print("Removed " + boss_role_name + " from the user " + member.name)
        boss_queries.delete_user_queue("boss5", server.id, discord_username, int(boss_round))

    else:
        print("Foreign Emoji has been added to the message - ignoring")
