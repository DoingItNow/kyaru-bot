"""
    File containing functions that is used to create messages for boss queues
"""

import discord

from queries import message_queries

async def send_cb_queue_message(queue_number, server, member, queue_channel, queue_type):
    """
        Sends the round message with reactions attached
    """
    server_id = server.id

    reaction_message_text = '**' + queue_type.capitalize() + ' ' + str(queue_number) + ' Bosses**\n' \
        'Click the emoji below to tag and reserve a spot for a particular boss.'

    admin_role = discord.utils.get(server.roles, name="Kyaru's Playground")
    kyaru_bot_role = discord.utils.get(server.roles, name="Kyaru")
    skyaru_bot_role = discord.utils.get(server.roles, name="Summer Kyaru")

    if (admin_role in member.roles or kyaru_bot_role in member.roles or skyaru_bot_role in member.roles) and queue_channel.name == "cb-boss-queues":
        reaction_message = await queue_channel.send(reaction_message_text)

        await reaction_message.add_reaction('1\N{COMBINING ENCLOSING KEYCAP}')
        await reaction_message.add_reaction('2\N{COMBINING ENCLOSING KEYCAP}')
        await reaction_message.add_reaction('3\N{COMBINING ENCLOSING KEYCAP}')
        await reaction_message.add_reaction('4\N{COMBINING ENCLOSING KEYCAP}')
        await reaction_message.add_reaction('5\N{COMBINING ENCLOSING KEYCAP}')

        queue_type_quotes = "'%s'" % queue_type
        message_queries.insert_messages_round_entry(int(reaction_message.id), int(server_id), queue_number, queue_type_quotes)
