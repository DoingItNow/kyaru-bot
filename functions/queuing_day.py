"""
    File containing functions that uses the queuing system where it is done as
    "Boss Queue By Day"
"""

import discord

from functions import get_bot_member
from functions import queuing_messages
from functions import automated_mod_roles

from queries import message_queries

async def create_next_messages(bot_user, server, queue_number, queue_type):
    """
        Create the next reaction messages based on the day
    """
    members = server.members
    queue_channel = discord.utils.get(
        server.channels, name="cb-boss-queues")

    bot_member = get_bot_member.get_bot_as_member(bot_user, members)

    await queuing_messages.send_cb_queue_message(queue_number, server, bot_member, queue_channel, queue_type)

async def edit_dead_boss_messages(server, old_day_number):
    """
        Edit the queue messages to strikethru the message for that day
    """

    message_id = message_queries.get_queue_message_id(server.id, old_day_number)

    editted_message = '~~**Day ' + str(old_day_number) + ' Bosses**\n' \
        'Click the emoji below to tag and reserve a spot for a particular boss.~~'

    queue_channel = discord.utils.get(server.channels, name="cb-boss-queues")
    boss_message = await queue_channel.fetch_message(message_id)

    await boss_message.edit(content=editted_message)

async def add_day_boss_roles(bot, server, queue_type):
    """
        Adds bosses 1 to 5 for a specific day to Kyaru

        This will only be run if the bosses are not added to Kyaru before
    """
    boss_server_list = automated_mod_roles.get_server_boss_roles(server, queue_type)
    bot_boss_roles = automated_mod_roles.get_bot_boss_roles(bot, server.members, queue_type)

    if bot_boss_roles != boss_server_list:
        bot_member = get_bot_member.get_bot_as_member(bot.user, server.members)
        for boss in boss_server_list:
            await bot_member.add_roles(boss)
            print("Added " + boss.name + " to Kyaru")
