"""
Function that gets the bot as a member of the server
"""

def get_bot_as_member(user, members):
    """
        Returns the bot as a member of the server
    """
    bot_member = None
    for member in members:
        if member.id == user.id:
            bot_member = member
    return bot_member

def convert_time(time):
    """
        Converts time into int
    """
    time_split = time.split(":")
    hour = int(time_split[0])
    minute = int(time_split[1])
    total_time = 60 * hour + minute

    return total_time
    