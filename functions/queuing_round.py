"""
    File containing functions that uses the queuing system where it is done as
    "Boss Queue By Round"
"""

import discord

from functions import get_bot_member
from functions import queuing_messages

from queries import message_queries
from queries import boss_queries

async def delete_dead_boss_roles(server, bot_boss_roles):
    """
        Deletes the boss roles that the bot has which is considered "dead"
    """
    if bot_boss_roles:
        dead_boss_list = []
        for role in bot_boss_roles:
            await role.delete()
            print("Deleting the role " + role.name)
            dead_boss_list.append(role.name)

            # Clean up the DB entries of the users in queue
            boss_role_split_list = role.name.split("-")
            queue_number = boss_role_split_list[1]
            boss_number = boss_role_split_list[3]

            boss_name = "boss" + boss_number
            boss_queries.delete_queue_daily(boss_name, server.id, queue_number)
        dead_boss_list.sort()

        log_channel = discord.utils.get(server.channels, name="kyaru-playground")
        await log_channel.send("The following roles were removed after reset" + ":\n" + "```\n" + "\n".join(dead_boss_list) + "```")
    else:
        print("No roles were needed to be deleted for " + server.name)


async def edit_dead_boss_messages(server, queue_numbers):
    """
        Edit the queue messages to strikethru the messages if all bosses are dead
        day_round_number is a list of number that can be either day or round numbers
    """
    if queue_numbers is not None:
        for queue_number in queue_numbers:
            message_id = message_queries.get_queue_message_id(server.id, queue_number)

            editted_message = '~~**Round ' + str(queue_number) + ' Bosses**\n' \
                'Click the emoji below to tag and reserve a spot for a particular boss.~~'

            queue_channel = discord.utils.get(server.channels, name="cb-boss-queues")
            boss_message = await queue_channel.fetch_message(message_id)

            await boss_message.edit(content=editted_message)
    else:
        print("No messages were needed to be editted")

async def create_next_messages(bot_user, server, next_numbers, queue_type):
    """
        Create the next reaction messages based of the day or round
    """
    members = server.members
    queue_channel = discord.utils.get(
        server.channels, name="cb-boss-queues")

    bot_member = get_bot_member.get_bot_as_member(bot_user, members)

    for day_round_number in range(next_numbers[0], next_numbers[1]+1):
        await queuing_messages.send_cb_queue_message(day_round_number, server, bot_member, queue_channel, queue_type)
