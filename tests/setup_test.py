"""
Unit tests for setup.py
"""
import unittest
from config import setup
from .mocks import discord_mocks


class SetupTests(unittest.TestCase):
    """Class that defines all the test cases
    """

    def test_login(self):
        """Test to see if client details are properly given
        """
        client = discord_mocks.MockBot("Kyaru")
        expected_list = ["Kyaru", "0987654321"]
        actual_list = setup.get_client_details(client)
        self.assertEqual(expected_list, actual_list)
